#!/usr/bin/env python3
#!/usr/bin/python3

import logging
import schedule
import time
import zope.event
from threading import Thread

import event
from event import *

import driver
import light as lightmodel
import solarsystem
import config
import digits

minimum_power = 0.8
minimum_color_temp = config.light["minimum_temp"]
logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

def adjust_hour(alarmtime, diff):
    """
    Adjust the hour by an amount of hour, tens minutes, or minutes
    'diff' is a 3-tuple of intenger containing (hour, tens minutes, minute)
    """
    hour = alarmtime[3]
    minute = alarmtime[4]
    hour = int((hour + diff.x) % 24)
    minute =int((minute + diff.y*10 + diff.z + 60) % 60)
    wr = list(alarmtime)
    wr[3] = hour
    wr[4] = minute
    return time.struct_time(tuple(wr))

def string_to_time(hourstring):
    hour = time.strptime(hourstring, "%H:%M")
    return hour

def tuple_to_time(timetuple):
    return timetuple[3] + timetuple[4]/60.0 + timetuple[5]/3600.0

def tuple_to_str(timetuple):
    minutes =  timetuple[4]
    hour =  timetuple[3]
    if minutes < 10 : minutes = '0{}'.format(minutes)
    if hour < 10 : hour = '0{}'.format(hour)
    timestr = '{}:{}'.format(hour, minutes)
    return timestr


class Controller:

    def __init__(self):
        self.light = lightmodel.Light.black_light()
        zope.event.subscribers.append(self.input)
        self.my_driver = driver.Driver()
        self.digits_driver = digits.Digits()
        self.solarsystem = solarsystem.SolarSystem()
        self.on = False
        self.waking = False
        self.manual = False
        self.alarm_time = string_to_time(config.sleep["wakeup"])
        self.sleep_time = string_to_time(config.sleep["nighttime"])
        self.update_daylight()
        schedule.every().day.at(tuple_to_str(self.alarm_time)).do(self.update_daylight)
        self.alarm = False
        self.default_transition()

    def input(self, evt):
        """
        Receive and treat events thrown by input devices
        """
        logging.info('got: ' + str(evt) +  " in " + str(self.context))
        if (self.context == Context.DEFAULT):
            self.default_input(evt)
        elif(self.context == Context.ALARMSETTING):
            self.alarmset_input(evt)

    def default_input(self, evt):
        if(evt.action == Action.SWITCH
                or evt.action == Action.PLAYPAUSE):
            self.switch()
        if(evt.action == Action.COLOR_CHANGE):
            self.turn(True, evt.color)
        if(evt.action == Action.BRIGHT_CHANGE):
            self.adjust(evt.color
            .multiply(config.light["ir_step"]))
        if(evt.action == Action.L_QUICK):
            self.switch_alarm(not self.alarm)
        if(evt.action == Action.L_SLOW):
            self.switch_alarm(True)
            self.alarmset_transition()

    def alarmset_input(self, evt):
        if (evt.action == Action.PLAYPAUSE
                or evt.action == Action.SLOW):
            self.default_transition()
            self.update_daylight()
        if(evt.action == Action.L_QUICK):
            self.switch_alarm(not self.alarm)
            self.default_transition()
        if(evt.action == Action.BRIGHT_CHANGE):
            self.alarm_time = adjust_hour(self.alarm_time, evt.color)
            self.digits_driver.update_alarmtime(self.alarm_time)


    def default_transition(self):
        self.context = Context.DEFAULT
        self.digits_driver.default_display()

    def alarmset_transition(self):
        self.context = Context.ALARMSETTING
        self.digits_driver.alarmsetting_display(self.alarm_time)

    def switch_alarm(self, on):
        schedule.clear('alarm')
        self.alarm = on
        self.digits_driver.setAlarmIndicator(on)
        if (on):
            logging.info("Scheduling alarm at " + tuple_to_str(self.alarm_time))
            schedule.every().day.at(tuple_to_str(self.alarm_time)).do(self.wakeup).tag('alarm')

    def update_daylight(self, alarm_time = None, sleep_time = None):
        """
        schedule alarm time and use time to evaluate wake up time and
        adjust parameters for color temperature and light strength
        """
        if (alarm_time != None): self.alarm_time = alarm_time
        if (sleep_time != None): self.sleep_time = sleep_time
        alarmhour = tuple_to_time(self.alarm_time)
        sleephour = self.solarsystem.sunset();
        daylength = sleephour - alarmhour + 1
        if daylength < 0:
            daylength += 24
            logging.warning('daylength negative')
        declination = self.solarsystem.declination_from_daylength(daylength)
        self.solarsystem.set_position(declination, alarmhour + daylength/2)
        logging.info('declination : ' + str(declination))

    def display(self):
        """
        Update the light displayed with the light member"
        """
        self.my_driver.display(self.light)

    def wakeup(self):
        """
        Turns on the light at wake up time
        """
        logging.info("Waking up")
        self.waking = True
        schedule.every(2).hours.do(self.end_wakeup)
        self.turn(True)

    def end_wakeup(self):
        self.waking = False
        return schedule.CancelJob

    def switch(self):
        """
        Switch light on or off
        """
        self.turn( not self.on)

    def adjust(self, color):
        self.light.set_adjusted_color(color)
        self.on = self.on and self.light.is_on()
        self.manual = self.on
        self.display()

    def turn(self, on, color=None):
        """
        Turn light on (or off).
        Use the auto color temp if no 'color' is specified
        """
        if not self.on and on :
            hour = tuple_to_time(time.localtime())
            self.update_light(hour)
        if on and color is not None:
            self.light.turn_to_rgb(color)
        elif not on :
            self.light.turn_to_light(lightmodel.Light.black_light())
        self.on = on
        self.manual = color is not None and on
        logging.info('Turning on to ' + str(self.light))
        self.display()

    def update_light(self, hour):
        """
        Update light member with auto color temp and strength
        """
        (elevation, irr, color) = self.solarsystem.get_sun_char(hour)
        if not self.waking:
            irr = max(irr, minimum_power)
            color = max(color, minimum_color_temp)
        self.light.turn_to_temp(color, irr)
        if hour*6 == int(hour*6):
            logging.info(str(color) + ' ' +  str(irr))

    def main(self):
        while True:
            schedule.run_pending()
            if self.on and not self.manual:
                hour = tuple_to_time(time.localtime())
                self.update_light(hour)
                self.display()
            time.sleep(60)


if __name__ == '__main__':
   testalarm = time.localtime(time.time() + 60)
   testsleep = time.localtime(time.time() + 3600)
   minutes =  testalarm[4]
   hour =  testalarm[3]
   controller = Controller()
   controller.update_daylight(alarm_time = testalarm, sleep_time = testsleep)
   controller.switch_alarm(True)
   controller.main()
