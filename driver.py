import serial
import light
import time
import math
import config

M = 255

class Driver:


    def __init__(self):
        self.ser = serial.Serial('/dev/ttyACM0', 19200)

    def display(self, lights):
        rgb = lights.getRGB().gammaCorrect()
        self.ser.write(('<' + str(int(rgb.red*M)) + ',' +
                str(int(rgb.green*M)) + ',' +
                str(int(rgb.blue*M)) + '>'
                ).encode('utf-8')
                )


if __name__ == '__main__':
    driver = Driver()
    lights = light.Light.black_light()
    red =  input("red") or 0
    green = input("green") or 0
    blue = input("blue") or 0
    lights.turn_to_rgb((int(red), int(green), int(blue)))
    driver.display(lights)
    input("Enter to continue.") 
    
    for ct in range(1000, 6000, 100):
        lights.turn_to_temp(ct, 1)
        driver.display(lights)
        print(ct, ' : ' , lights)
        time.sleep(1)
    input("Enter to continue.") 
    driver.display(light.Light.black_light())

