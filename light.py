import logging
import math
from vectors import Vector
import config
import colorsys
from rgb import RGB

logging.basicConfig(filename='sunrise.log', format='%(asctime)s %(message)s', level=logging.DEBUG)

def hsv_to_hls(h, s, v):
    l = v  * (2 - s)
    s = v * s 
    if l == 0 or l == 2:
        pass
    elif l <= 1 :
        s /= l
    else :
        s /= 2 - l
    l /= 2
    return [h, l, s]

class Light:
    fade = None
    default = None

    def __init__(self, h, l, s):
        self.hls = Vector(h, l, s)
        self.fade = None

    def __str__(self):
        return "Light: hls " + str(self.hls) 

    def getRGB(self):
        return RGB(*colorsys.hls_to_rgb(*self.hls.to_points()))

    def black_light():
        return Light(0, 0, 0)

    def is_on(self):
        return self.hls.y > 0.05 

    def adjust_HLS(self, diff):
        self.hls = self.hls.sum(diff)
        self.hls.x = self.hls.x % 1
        l = self.hls.to_points()
        for i in range(3):
            f = l[i]
            f = min(f, 1)
            f = max(f, 0)
            l[i] = f
        self.hls = Vector.from_list(l)

    def set_adjusted_color(self, diff):
        self.adjust_HLS(diff)
        print(self.hls)
        return self.hls

    def turn_to_light(self, light):
        self.hls = light.hls

    def turn_to_rgb(self, rgb):
        z = colorsys.rgb_to_hsv(*rgb.to_list())
        self.hls.x = z[0]
        self.hls.z = z[1]

    def turn_to_temp(self, ct, intensity):
        r = RGB.temp_to_rgb(ct)
        hsv = colorsys.rgb_to_hsv(*r)
        self.hls = Vector.from_list(
                hsv_to_hls(hsv[0], hsv[1],hsv[2]*intensity)
                )

    def set_fade(self, light):
        self.fade = light
