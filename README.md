# Sunrise clock
This project is a sunrise clock and a RGB light.
It is design to fill a room with gentle warm light in the morning and evening, a bright white light during the day or any other color picked.
Its "minimalist build can be controlled by a single push button but it's possible to add more buttons, a infrared remote and a trackpad for more functionnality.

## Hardware used

* a raspberry pi
* 3 irlb8721 (one per color channel)
* a 32u4 based Arduino board or any arduino board if not using a trackpad
* some push buttons
* a RGB led strip
* a 12V power supply, check the amps needed according to your LED strip
* a pi power supply, I used the 12V ones and and a 12V to 5V, 2.5A converter
* a USB cable to connect the pi and the arduino
* wires, breadboard, soldering kit, ...
**Optionnal**
* a salvaged trackpad from a laptop
* a tm1637 display
* a IR receiver and remote


## Installation

Wire the push buttons to the pi, see [here](https://raspberrypihq.com/use-a-push-button-with-raspberry-pi-gpio/) for more details.
By default the button to switch the light is wired to GPIO16, the one to change the hue to GPIO27, brightness up and down to GPIO24 and GPIO 23 respectively.

Install `trackpad/trackpad_and_pwm.ino` to the arduino.
Make the trackpad work with the Arduino, [see more](https://hackaday.com/2017/07/19/raspberry-pi-trackpad-from-salvaged-trackpad-plus-arduino/).

Wire the arduino to the IRLB8721, led strip and 12V power supply, see [Adafruit tutorial](https://learn.adafruit.com/rgb-led-strips/usage).
Default config uses pin 3 for red, pin 5 for green, pin 6 for blue.
Connecting the arduino to the computer at this point, you should be able to send triplets in the form `<red, green, blue>` through the Arduino's IDE serial monitor to the Arduino to light up the strip.
Also the trackpad should be able to move your computer cursor

Connect the pi and arduino via USB. If using a trackpad you might want to reboot the pi and check the trackpad is really the device `/dev/input/event0` (executing `sudo cat /dev/input/event0` should display a bunch of garbage when using the touchpad).

Set up the pi to use the IR receiver and wire accordingly (see [here](https://blog.gordonturner.com/2020/05/31/raspberry-pi-ir-receiver/) to set up and use ir-keytable)
To my knowledge the `ir-remote.toml` provided in this repository can be used for most 44 key remotes that can be found to control LEDs on the market. The labelling on the keys might change from one to the other though.

Wire the TM1637 to the Pi, default config uses GPIO5 for clock and GPIO4 for data

Using the trackpad or an IR remote, we need to run the sunrise clock code with super user priviledges so we're going to install everything taking that into account.
Install dependancies on the pi: `sudo pip3 install keyboard evdev pyserial raspberrypi-python-tm1637 vectors zope.event schedule pigpio pyserial`
Copy `config.py.default` to `config.py`. You can modify `config.py` to take into account if you installed an IR receiver or trackpad for exemple. It's also where the pinout can be modified if needed.

Run `startsunriseclock.sh`. You can also make it execute when booting

## Support
Feel free to mail me or open an issue if you run in any problem.
