
import wiredbutton
import irremote
import trackpad
import controller
import config
from threading import Thread

control = controller.Controller()
if(config.devices["button"]) :
  button = wiredbutton.Buttons()
  wiredbtnprocess = Thread(target=button.main)
  wiredbtnprocess.start()
if(config.devices["infrared"]) :
  ir_thread = Thread(target=irremote.main)
  ir_thread.start()
if(config.devices["trackpad"]) :
  mouse_thread = Thread(target=trackpad.main)
  mouse_thread.start()
control.switch_alarm(True)
control.main()
