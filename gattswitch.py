"""
    samples.subscribe_indicate_thermometer_sample
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    This is an example of subscribing to an indicate property of a
    characteristic. This example was tested with the Health Thermometer Profile,
    but can be easily modified to work with any other profile.
"""
import pygatt
from pygatt import BLEDevice
from pygatt.exceptions import NotConnectedError
import logging
import time
import zope.event
import event
from event import *


def data_handler_cb(handle, value):
    """
        Indication and notification come asynchronously, we use this function to
        handle them either one at the time as they come.
    :param handle:
    :param value:
    :return:
    """
    print("Data: {}".format(value.hex()))
    print("Handle: {}".format(handle))
    if (value) == bytearray([0xcd, 0x00]): 
           event = Event(Action.SWITCH)
    elif (value) == bytearray([0x00, 0x00]): 
           event = Event(Action.RELEASE)
    else :
           event = Event(Action.UNKNOWN)
    zope.event.notify(event)


def main():
    """
        Main function. The comments below try to explain what each section of
        the code does.
    """

    # pygatt uses pexpect and if your device has a long list of characteristics,
    # pexpect will not catch them all. We increase the search window to
    # 2048 bytes for the this example. By default, it is 200.
    # Note: We need an instance of GATToolBackend per each device connection
    #logging.basicConfig()
    #logging.getLogger('pygatt').setLevel(logging.DEBUG)
    adapter = pygatt.GATTToolBackend(search_window_size=2048)

    try:
        # Start the adapter
        adapter.start()
        connected = False
        # Connect to the device with that given parameter.
        # For scanning, use adapter.scan()
        while not connected:
            try:
                device = adapter.connect("FF:FF:00:05:EA:FF", auto_reconnect=True)
                connected = True
            except NotConnectedError:
                connected = False
                time.sleep(5)
        # Set the security level to medium
        device.bond()
        # Observes the given characteristics for indications.
        # When a response is available, calls data_handle_cb
        uuid="00002a19-0000-1000-8000-00805f9b34fb"
        device.subscribe(uuid, callback=data_handler_cb)
        while True:
           time.sleep(1)
    finally:
        adapter.stop()

    return 0


if __name__ == '__main__':
    exit(main())
