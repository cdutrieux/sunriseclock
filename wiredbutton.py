#!/usr/bin/env python
import zope.event
from signal import pause
import time
from vectors import Vector

import config
import event
import pigpio
from stoppablethread import StoppableThread

threads = dict();
events = dict();

class Buttons:

    def __init__(self):
        step = config.light["ir_step"]
        events[config.pin["b_brightup"]] = event.Event(
                event.Action.BRIGHT_CHANGE,
                color = Vector(0, 1, 0).multiply(step)
                )
        events[config.pin["b_brightdown"]] = event.Event(
                event.Action.BRIGHT_CHANGE,
                color = Vector(0, -1, 0).multiply(step)
                )
        events[config.pin["b_hue"]] = event.Event(
                event.Action.BRIGHT_CHANGE,
                color = Vector(1, 0, 0).multiply(step)
                )
        

    def switch_callback(gpio, level, tick):
        zope.event.notify(event.Event(event.Action.SWITCH))

    def stop_thread(gpio):
        if gpio in threads:
            threads[gpio].stop()
            threads[gpio].join()
            del threads[gpio]

    def buttons_callback(gpio, level, tick):
        def thread_action():
            zope.event.notify(events[gpio])
            time.sleep(.15)

        if level == 0:
            Buttons.stop_thread(gpio)
            st = StoppableThread()
            st.set_action(thread_action)
            st.start()
            threads[gpio] = st
        if level == 1:
            Buttons.stop_thread(gpio)

    def main(self):
        pi = pigpio.pi() # connect to Pi
        if not pi.connected:
            exit(0)
        cbs = []
        BUTTON = config.pin["b_switch"]
        pi.set_mode(BUTTON, pigpio.INPUT)
        pi.set_pull_up_down(BUTTON, pigpio.PUD_UP)
        pi.set_glitch_filter(BUTTON, 500) # no glitch filter
        cb = pi.callback(BUTTON, pigpio.RISING_EDGE, Buttons.switch_callback)
        cbs.append(cb)
        for pin in events:
            pi.set_mode(pin, pigpio.INPUT)
            pi.set_pull_up_down(pin, pigpio.PUD_UP)
            pi.set_glitch_filter(pin, 500) # no glitch filter
            cb = pi.callback(pin, pigpio.EITHER_EDGE, Buttons.buttons_callback)
            cbs.append(cb)
        pause()
        for c in cbs:
            c.cancel() # cancel callback
        for gpio in threads:
            Button.stop_thread(gpio)
        pi.stop() # disconnect from Pi

if __name__ == '__main__':
      buttons = Buttons()
      buttons.main()
